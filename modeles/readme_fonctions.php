<?php
if (!defined('_ECRIRE_INC_VERSION')){
	return;
}


/**
 * Récupère le contenu d'un fichier readme distant
 *
 * @param string $plugin : nom du plugin (nom du dossier dans gitea)
 * @param string $url : (facultatif) url de base de la forge, par défaut : https://git.spip.net/spip-contrib-extensions
 * @param string $branche (facultatif) par défaut : main
 * @param string $fichier (facultatif) nom du fichier par défaut : README.md
 *
 * @use copie_locale()
 * $use markdown_propre()
 *
 * @return string
 */
function show_readme($plugin, $url = '', $branche = 'main', $fichier = 'README.md' ): string {

	// https://git.spip.net/spip-contrib-extensions/show_readme/src/branch/master/modeles/readme.html
	// https://git.spip.net/spip-contrib-extensions/show_readme/raw/branch/master/README.md
	if (empty($plugin)) {
		return '';
	}
	$url_raw =  $url ?:'https://git.spip.net/spip-contrib-extensions' . '/' . $plugin . '/raw/branch/' . $branche;
	$url_src =  $url ?:'https://git.spip.net/spip-contrib-extensions' . '/' . $plugin . '/src/branch/' . $branche;
	$url_fichier =  $url_raw . '/' . $fichier;

	$mode = 'auto';
	if (_request('var_mode') === 'calcul') {
		$mode = 'modif';
	}

	include_spip('inc/distant');
	$url_readme = copie_locale($url_fichier, $mode);
	if (!$url_readme) {
		spip_log('Echec d’obtention du README : ' . $url, _LOG_INFO);
		return '';
	}

	$content = '';
	if (file_exists(_DIR_RACINE . $url_readme)) {
		lire_fichier(_DIR_RACINE . $url_readme, $content);
	}
	if (!$content) {
		spip_log('Echec lecture du README : ' . $url_readme, _LOG_INFO);
		return '';
	}

	$content = md_parse_urls($content, $url_src, $url_raw);

	return markdown_propre($content);
}

function md_parse_urls(string $content, string $url_src, string $url_raw):string {
	$url_src = rtrim($url_src, '/') . '/';
	$url_raw = rtrim($url_raw, '/') . '/';
	$regex = <<<REGEX
	/
	(?<image>[!]?)           # image ou simple lien ?
	\[(?<title>[^\[\]]*)\]   # title: [content]
	\((?<link>[^\(\)]*)\)    # link: (to file.ext) or (https...)
	/x
	REGEX;
	if (preg_match_all($regex, $content, $matches, \PREG_SET_ORDER)) {
		foreach ($matches as $m) {
			$link = trim($m['link']);
			if (
				empty($link)
				|| str_starts_with($link, 'http')
				|| str_contains($link, '://')
			) {
				continue;
			}
			$link = ltrim($link, './');
			if ($m['image']) {
				$new = '![' . $m['title'] . '](' . $url_raw . $link . ')';
			} else {
				$new = '[' . $m['title'] . '](' . $url_src . $link . ')';
			}
			$content = str_replace($m[0], $new, $content);
		}
	}

	return $content;
}

