# Show README.md

![readme](./readme.svg)

Petit plugin qui apporte simplement un modèle qui permet d'afficher le README d'un plugin SPIP

```
<readme|plugin=vue_vite>
<readme|plugin=saisies|branche=master>
```

## Fonctionnement

`version 0.0.1` :
* Afin de ne pas faire un hit à chaque chargement de page, le modèle met en cache le contenu du fichier récupéré. Si l'on souhaite forcer la mise à jour, il faut passer
`&var_mode=calcul`
