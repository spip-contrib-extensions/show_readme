<?php

namespace Spip\Test\Plugin\ShowReadme;

use PHPUnit\Framework\Attributes\CoversFunction;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

#[CoversFunction('md_parse_urls')]
class MdParseURlTest extends TestCase {
    public static function setUpBeforeClass(): void {
        require_once dirname(__DIR__) . '/modeles/readme_fonctions.php';
    }

    public static function providerMdParseUrl() {
        $url_src = 'https://domain.tld/src/';
        $url_raw = 'https://domain.tld/raw/';
        $data = [
            'n’est pas un lien markdown' => [
                'source' => '(demo.jpg)',
                'expected' => '(demo.jpg)',
            ],
            'est un lien vide' => [
                'source' => '[lien]()',
                'expected' => '[lien]()',
            ],
            'est une image vide' =>  [
                'source' => '![image]()',
                'expected' => '![image]()',
            ],
            'est un lien absolu' => [
                'source' => '[lien](http://domain.tld/document)',
                'expected' => '[lien](http://domain.tld/document)',
            ],
            'est un lien absolu https' => [
                'source' => '[lien](https://domain.tld/document)',
                'expected' => '[lien](https://domain.tld/document)',
            ],
            'est un lien absolu autre schema' => [
                'source' => '[lien](hello://domain.tld/document)',
                'expected' => '[lien](hello://domain.tld/document)',
            ],
            'est une image absolue' => [
                'source' => '![image](http://domain.tld/image.jpg)',
                'expected' => '![image](http://domain.tld/image.jpg)',
            ],
            'est une image absolue https' => [
                'source' => '![image](https://domain.tld/image.jpg)',
                'expected' => '![image](https://domain.tld/image.jpg)',
            ],
            'est une image absolue autre schema' => [
                'source' => '![image](hello://domain.tld/image.jpg)',
                'expected' => '![image](hello://domain.tld/image.jpg)',
            ],
            'est un lien relatif' => [
                'source' => '[lien](/document)',
                'expected' => '[lien](https://domain.tld/src/document)',
            ],
            'est un lien relatif sans slash' => [
                'source' => '[lien](document)',
                'expected' => '[lien](https://domain.tld/src/document)',
            ],
            'est un lien relatif point slash' => [
                'source' => '[lien](./document)',
                'expected' => '[lien](https://domain.tld/src/document)',
            ],
            'est une image relative' => [
                'source' => '![image](/image.png)',
                'expected' => '![image](https://domain.tld/raw/image.png)',
            ],
            'est une image relative sans slash' => [
                'source' => '![image](image.png)',
                'expected' => '![image](https://domain.tld/raw/image.png)',
            ],
            'est une image relative point slash' => [
                'source' => '![image](./image.png)',
                'expected' => '![image](https://domain.tld/raw/image.png)',
            ],
        ];
        return array_map(fn($d) => [...$d, 'url_src' => $url_src, 'url_raw' => $url_raw], $data);
	}

	#[DataProvider('providerMdParseUrl')]
	public function testUrlRelative($source, $expected, $url_src, $url_raw) {
		$actual = md_parse_urls($source, $url_src, $url_raw);
        $this->assertSame($actual, $expected);
	}
}